package xorm

import (
	"github.com/modern-go/reflect2"
	"xorm.io/xorm/internal/json"
)

func SetDefaultJSONHandler(handler json.Interface) {
	if reflect2.IsNil(handler) {
		json.DefaultJSONHandler = handler
	}
}
